use chrono::prelude::*;
use flexi_logger::{opt_format, Logger};
use i3ipc::event::{BindingEventInfo, Event as I3Event};
use i3ipc::{I3EventListener, Subscription};
use log::*;
use log_panics;
use serde::Serialize;
use serde_json;
use signal_hook::flag as signal_flag;
use std::env;
use std::fmt::{self, Display};
use std::io::{self, Write};
use std::panic;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;
use subprocess::{Popen, PopenConfig};
use sensors::Sensors;
use sensors::Subfeature;

// TODO: Implement i3 Event Handling
// TODO: Implement stdin (mouse) handling
// TODO: Decouple code
//

enum SplitInfo {
    Vertical,
    Horizontal,
    Toggled,
    Unknown(String),
}

impl Display for SplitInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            SplitInfo::Vertical => write!(f, "{}", "Vertical"),
            SplitInfo::Horizontal => write!(f, "{}", "Horizontal"),
            SplitInfo::Toggled => write!(f, "{}", "Toggled"),
            SplitInfo::Unknown(string) => write!(f, "{}", &string),
        }
    }
}

fn sys_notify(title:&str, msg: &str) {
    if let Err(err) = Popen::create(
        &[
        "notify-send",
        title,
        msg,
        "--icon=window-notify",
        ],
        PopenConfig::default(),
        )
    {
        error!("Popen failed: {:?}", err);
    }
}

fn cmd_split<'a>(param: Option<&'a str>) {
    let new_state = match param {
        Some(cmdstr) => {
            let parts: Vec<&str> = cmdstr.split(';').collect();
            let mut result = SplitInfo::Unknown(String::default());
            for p in parts {
                if p.find('v').is_some() {
                    result = SplitInfo::Vertical;
                    break
                }
                else if p.find('h').is_some()
                {
                    result = SplitInfo::Horizontal;
                    break
                }
                else if p.find("toggle").is_some()
                {
                    result = SplitInfo::Toggled;
                    break
                }
                else {
                    result = SplitInfo::Unknown("?".to_owned())
                }
            }
            result
        }
        _ => SplitInfo::Unknown("Wat".to_owned())
    };
    sys_notify("Layout Event", &new_state.to_string());
}

fn handle_binding(evt: &BindingEventInfo) {
    let txt = format!("binding event: {:?}", &evt);
    debug!("{}",&txt);
    let mut cmd_parts = evt.binding.command.split_whitespace();
    match cmd_parts.next() {
        Some("split") => cmd_split(cmd_parts.next()),
        _ => ()//sys_notify("Binding", &txt),
    }
}

fn events() {
    // establish connection.
    let mut listener = I3EventListener::connect().unwrap();

    // subscribe to a couple events.
    let subs = [
        Subscription::Mode,
        Subscription::Binding,
        Subscription::Window,
        Subscription::Workspace,
    ];
    listener.subscribe(&subs).unwrap();

    // handle them
    for event in listener.listen() {
        match event.unwrap() {
            I3Event::ModeEvent(e) => debug!("mode event: {:?}", e),
            I3Event::BindingEvent(e) => handle_binding(&e),
            I3Event::WindowEvent(e) => debug!("window event: {:?}", e),
            I3Event::OutputEvent(e) => debug!("output event: {:?}", e),
            I3Event::BarConfigEvent(e) => debug!("config event: {:?}", e),
            I3Event::WorkspaceEvent(e) => debug!("workspace event: {:?}", e),
            I3Event::ShutdownEvent(e) => debug!("shutdown event: {:?}", e),
        }
    }
}

#[derive(Debug, Clone, Serialize)]
struct DataBlock {
    full_text: String,
    markup: &'static str,
}

impl DataBlock {
    pub fn new(content: &str) -> DataBlock {
        DataBlock {
            full_text: String::from(content),
            markup: "pango",
        }
    }
}

//TODO: Don't render all blocks at the same rate
fn render(blocks: &Vec<DataBlock>) {
    println!(
        ",{json}",
        json = serde_json::to_string(&blocks).unwrap_or_else(|_| String::from("ERROR"))
    );
}

fn terminate() {
    println!(",]");
}

fn clock(local_time: &DateTime<Local>) -> DataBlock {
    DataBlock::new(&format!("<b>{}</b>", local_time.format("%H:%M")))
}

fn date_output(local_time: &DateTime<Local>) -> DataBlock {
    DataBlock::new(&format!("<b>{}</b>", local_time.format("%a %d.%m")))
}

fn cpu_temp(temp: &(Option<Subfeature>,Option<Subfeature>)) -> DataBlock {
    if temp.0.is_none() || temp.1.is_none() {
        return DataBlock::new("<b>No sensor found!</b>");
    }
    let currel_temp = temp.0.as_ref().unwrap().get_value().unwrap();
    let maxrel_temp = temp.1.as_ref().unwrap().get_value().unwrap();
    let multiplier = 100.0/maxrel_temp;
    let temp = ((currel_temp * multiplier) / 2.0).trunc() as usize;
    let templeft = 50 - temp;
    DataBlock::new(&format!("<b>CPU</b> 🔥<span font='8' foreground='red'>┗{}</span><span font='8' foreground='gray'>{}┛</span>","━".repeat(temp), "━".repeat(templeft)))
}

fn assemble(blocks: &mut Vec<DataBlock>, temp: &(Option<Subfeature>,Option<Subfeature>)) {
    let local: DateTime<Local> = Local::now();
    blocks.push(cpu_temp(&temp));
    //blocks.push(date_output(&local));
    //blocks.push(clock(&local));
}

fn search_cpu_temp(sensors: &Sensors) -> (Option<Subfeature>,Option<Subfeature>)
{
    let mut out: (Option<Subfeature>,Option<Subfeature>) = (None, None);
    for chip in sensors.into_iter() {
        if chip.address() == 195 {
            for feature in chip {
                if feature.name() == "temp1" {
                    for subfeature in feature {
                        if subfeature.name() == "temp1_input" {
                            out.0 = Some(subfeature);
                        }
                        else if subfeature.name() == "temp1_max" {
                            out.1 = Some(subfeature);
                        }
                    }
                }
            }
        }
    }
    return out;
}

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let mut debug_settings = "info,tokio=warn";
    if args.len() >= 3 {
        let extra_cmd = &args[1];
        debug_settings = if extra_cmd == "--debug" {
            &args[2]
        } else {
            warn!("Invalid start arguments: {:?}", args);
            "info,tokio=warn"
        };
    }
    Logger::with_env_or_str(debug_settings)
        .log_to_file()
        .duplicate_to_stderr(flexi_logger::Duplicate::Debug)
        .directory("/tmp/a5status-logs")
        .format(opt_format)
        .start()
        .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));
    info!("A5 bar {}", env!("CARGO_PKG_VERSION"));
    log_panics::init();
    let stdout = io::stdout();
    let mut handle = stdout.lock();
    debug!("stdout lock acquired.");
    std::thread::sleep(std::time::Duration::from_secs(2));
    debug!("beginning now");
    std::thread::spawn(|| {
        events();
    });

    if let Err(error) = handle.write_all(b"{ \"version\": 1 }\n") {
        error!("Failed to send start message: {:#?}", error);
        std::process::exit(1)
    }
    if let Err(error) = handle.write_all(b"[\n[]\n") {
        error!("Failed to send clear message: {:#?}", error);
        std::process::exit(1)
    }

    let sys_signal = Arc::new(AtomicUsize::new(0));
    const SIGTERM: usize = signal_hook::SIGTERM as usize;
    const SIGINT: usize = signal_hook::SIGINT as usize;
    const SIGQUIT: usize = signal_hook::SIGQUIT as usize;
    const SIGSTOP: usize = signal_hook::SIGSTOP as usize;
    const SIGCONT: usize = signal_hook::SIGCONT as usize;
    signal_flag::register_usize(signal_hook::SIGTERM, Arc::clone(&sys_signal), SIGTERM)?;
    signal_flag::register_usize(signal_hook::SIGINT, Arc::clone(&sys_signal), SIGINT)?;
    signal_flag::register_usize(signal_hook::SIGQUIT, Arc::clone(&sys_signal), SIGQUIT)?;
    let sensors = Sensors::new();
    let temp:(Option<Subfeature>,Option<Subfeature>) = search_cpu_temp(&sensors);

    let mut running = true;
    let mut blocks: Vec<DataBlock> = Vec::new();
    loop {
        match sys_signal.load(Ordering::Relaxed) {
            0 => {
                if !running {
                    continue;
                }
                assemble(&mut blocks, &temp);
                render(&blocks);
                std::thread::sleep(std::time::Duration::from_secs(1));
                blocks.clear();
            }
            SIGTERM => {
                info!("Received SIGTERM... Forceful terminated");
                terminate();
                return Ok(());
            }
            SIGINT => {
                info!("Received SIGINT... Killed from console");
                terminate();
                return Ok(());
            }
            SIGQUIT => {
                info!("Received SIGQUIT...");
                terminate();
                return Ok(());
            }
            SIGSTOP => {
                info!("... pausing");
                running = false;
            }
            SIGCONT => {
                info!("... contiue");
                running = true;
            }
            _ => {
                warn!("Unsupported signal received: {:?}", sys_signal);
            }
        }
    }
}
