#!/bin/bash

ORG=$PWD
echo "Copy config..."
cp -vf ./config $HOME/.config/i3
cp -vf ./a5leave $HOME/.config/i3
cd a5status
echo "Building status bar..."
cargo build --release
echo "Copy a5status to /usr/local/bin"
sudo cp -vf target/release/a5status /usr/local/bin/a5status
echo "Done"
cd $ORG
