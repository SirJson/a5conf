#!/bin/bash

ORG=$PWD
cd a5status
echo "Kill status bar..."
pkill a5status
echo "Building status bar..."
cargo build --release
echo "Copy a5status to /usr/local/bin"
sudo cp -vf target/release/a5status /usr/local/bin/a5status
echo "Restart i3"
i3-msg restart
echo "Done"
cd $ORG
